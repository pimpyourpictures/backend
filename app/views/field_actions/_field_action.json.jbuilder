json.extract! field_action, :id, :name, :hide_default, :hide_invert_if_empty, :field_id
json.url template_group_field_field_action_url(field_action.field.group.template, field_action.field.group, field_action.field, field_action, format: :json)
