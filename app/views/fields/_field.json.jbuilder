json.extract! field, :id, :order, :name, :field_type_id, :optional, :qtmin, :qtmax, :group_id, :field_id
json.url template_group_field_url(field.group.template, field.group, field, format: :json)
