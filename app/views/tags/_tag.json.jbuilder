json.extract! tag, :id, :name, :highlighted
json.url tag_url(tag, format: :json)
