json.extract! variant, :id, :filename, :name, :description, :actif, :template_id
json.url template_variant_url(variant.template, variant, format: :json)
