json.extract! group, :id, :order, :name, :template_id, :fields
json.url template_group_url(group.template, group, format: :json)
