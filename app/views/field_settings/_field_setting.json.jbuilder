json.extract! field_setting, :id, :field_type_setting_type_id, :field_id, :value
json.url template_group_field_field_setting_url(field_setting.field.group.template, field_setting.field.group, field_setting.field, field_setting, format: :json)
