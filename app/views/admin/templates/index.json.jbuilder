json.enabled do
  json.array! @templates.where(actif: true) do |template|
    json.id template.id
    json.order template.order
    json.name template.name
    json.description template.description
    json.length template.length
    json.actif template.actif
  end
end

json.disabled do
  json.array! @templates.where(actif: false) do |template|
    json.id template.id
    json.order template.order
    json.name template.name
    json.description template.description
    json.length template.length
    json.actif template.actif
  end
end
