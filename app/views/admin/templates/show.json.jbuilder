json.current_template do
  json.id @template.id
  json.order @template.order
  json.name @template.name
  json.length @template.length
  json.description @template.description
  json.actif @template.actif
  if (@template.thumbnail.attached?)
    json.thumbnail rails_blob_url(@template.thumbnail)
  end
  if (@template.preview.attached?)
    json.preview rails_blob_url(@template.preview)
  end
  if (@template.projectfile.attached?)
    json.projectfile rails_blob_url(@template.projectfile)
  end
end

json.filed_variants @template.variants.where.not(filename: "") do |variant|
  json.id variant.id
  json.order variant.order
  json.filename variant.filename
  json.name variant.name
  json.description variant.description
  json.actif variant.actif
  json.resolutions do
    json.p360 !variant.resolutions.find_by(designator: "360p").nil?
    json.p720 !variant.resolutions.find_by(designator: "720p").nil?
    json.p1080 !variant.resolutions.find_by(designator: "1080p").nil?
  end
end

json.unfiled_variants @template.variants.where(filename: "") do |variant|
  json.id variant.id
  json.order variant.order
  json.name variant.name
  json.description variant.description
end

json.groups @template.groups do |group|
  json.id group.id
  json.order group.order
  json.name group.name
  json.variants_name group.variants.map{|variant| variant.filename}.join(",")
  json.variants group.variants.map{|variant| variant.id}.join(",")
  json.fields group.fields do |field|
    json.id field.id
    json.order field.order
    json.name field.name
    json.variants_name field.variants.map{|variant| variant.filename}.join(",")
    json.variants field.variants.map{|variant| variant.id}.join(",")
    json.optional field.optional
    json.qtmin field.qtmin
    json.qtmax field.qtmax
    json.field_type field.field_type.name
    json.possible_field_type_setting_types field.field_type.field_type_setting_types do |field_type_setting_type|
      json.value field_type_setting_type.id
      json.label field_type_setting_type.name
    end
    json.field_actions field.field_actions do |field_action|
      json.id field_action.id
      json.name field_action.name
      json.hide_default field_action.hide_default
      json.hide_invert_if_empty field_action.hide_invert_if_empty
    end
    json.field_settings field.field_settings do |field_setting|
      json.id field_setting.id
      json.value field_setting.value
      json.field_type_setting_type_id field_setting.field_type_setting_type.id
    end
  end
end

json.possible_field_types FieldType.all do |possibletype|
  json.value possibletype.id
  json.label possibletype.name
  json.field_type_setting_types possibletype.field_type_setting_types do |field_type_setting_type|
    json.value field_type_setting_type.id
    json.label field_type_setting_type.name
  end
end

json.possible_variants @template.variants.where.not(filename: "") do |possiblevariant|
  json.value possiblevariant.id
  json.label possiblevariant.filename
end
