json.extract! customisation, :id, :index, :value, :project_id, :field_id, :created_at, :updated_at
json.url project_customisation_url(customisation.project, customisation, format: :json)
