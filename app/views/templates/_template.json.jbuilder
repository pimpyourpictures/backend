json.id template.id
json.order template.order
if (template.thumbnail.attached?)
  json.thumbnail rails_blob_url(template.thumbnail)
end
if (template.preview.attached?)
  json.preview rails_blob_url(template.preview)
end
if (template.projectfile.attached?)
  json.projectfile rails_blob_url(template.projectfile)
end
json.name template.name
json.description template.description
json.length template.length
json.actif template.actif
json.variants template.variants do |variant|
  json.id variant.id
  json.order variant.order
  json.name variant.name
  json.filename variant.filename
  json.description variant.description
  json.actif variant.actif
  json.resolutions do
    json.p360 !variant.resolutions.find_by(designator: "360p").nil?
    json.p720 !variant.resolutions.find_by(designator: "720p").nil?
    json.p1080 !variant.resolutions.find_by(designator: "1080p").nil?
  end
end
json.groups template.groups do |group|
  json.variants group.variants.map{|variant| variant.filename}.join(",")
  json.possibleVariants template.variants.where.not(filename: "") do |possiblevariant|
    json.value possiblevariant.filename
    json.label possiblevariant.filename
  end
  json.possibleFieldTypes FieldType.all do |possibletype|
    json.value possibletype.id
    json.label possibletype.name
  end
  json.id group.id
  json.order group.order
  json.name group.name
  json.fields group.fields do |field|
    json.variants field.variants.map{|variant| variant.filename}.join(",")
    json.possibleVariants template.variants.where.not(filename: "") do |possiblevariant|
      json.value possiblevariant.filename
      json.label possiblevariant.filename
    end
    json.id field.id
    json.order field.order
    json.name field.name
    json.optional field.optional
    json.qtmin field.qtmin
    json.qtmax field.qtmax
    json.field_type do
      json.id field.field_type.id
      json.name field.field_type.name
    end
    json.field_settings field.field_settings do |field_setting|
      json.value field_setting.value
      json.field_type_setting_type do
        json.id field_setting.field_type_setting_type.id
        json.name field_setting.field_type_setting_type.name
        json.field_type_id field_setting.field_type_setting_type.field_type_id
      end
    end
  end
end
