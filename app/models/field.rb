class Field < ApplicationRecord
  belongs_to :field_type
  belongs_to :group
  belongs_to :field, optional: true

  has_one :template, :through => :group

  has_many :fields
  has_many :field_settings
  has_many :customisations
  has_many :field_actions
  has_many :variant_specifics, as: :variantable
  has_many :variants, through: :variant_specifics do
    def <<(new_item)
      super( Array(new_item) - proxy_association.owner.variants )
    end
  end

  def order_scope
    self.group.fields
  end
  include Rearrangeable

  def to_s
    name
  end
end
