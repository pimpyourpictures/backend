class FieldTypeSettingType < ApplicationRecord
  belongs_to :field_type

  has_many :field_settings

  def to_s
    name
  end
end
