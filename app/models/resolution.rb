class Resolution < ApplicationRecord
  has_and_belongs_to_many :variants do
    def <<(new_item)
      super( Array(new_item) - proxy_association.owner.variants )
    end
  end
end
