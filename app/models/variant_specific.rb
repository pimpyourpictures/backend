class VariantSpecific < ApplicationRecord
  belongs_to :variant
  belongs_to :variantable, polymorphic: true
end
