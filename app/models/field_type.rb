class FieldType < ApplicationRecord
  has_many :field_type_setting_types
  has_many :fields
  accepts_nested_attributes_for :field_type_setting_types

  def to_s
    name
  end
end
