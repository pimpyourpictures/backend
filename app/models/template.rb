class Template < ApplicationRecord
  has_one_attached :projectfile
  has_one_attached :thumbnail
  has_one_attached :preview
  has_many :variants
  has_many :groups

  def order_scope
    self.class
  end
  include Rearrangeable

  def to_s
    name
  end
end
