class FieldAction < ApplicationRecord
  belongs_to :field
  has_one :template, :through => :field
end
