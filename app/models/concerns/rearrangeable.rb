module Rearrangeable
  extend ActiveSupport::Concern

  included do
    before_validation :init
  end

  def init
    order_counter = 0
    while !self.order.present?  do
      if self.order_scope.where(order: order_counter).empty?
        self.order = order_counter
      else
        order_counter = order_counter+1
      end
    end
  end

  def rearrange(to)
    if self.order == to
      return
    elsif self.order > to
      self.order_scope.where(order: [to..self.order-1]).map(&:incorder)
      update_attribute(:order, to)
    else
      self.order_scope.where(order: [self.order+1..to]).map(&:decorder)
      update_attribute(:order, to)
    end
  end

  def incorder
    update_attribute(:order, self.order+1)
  end

  def decorder
    update_attribute(:order, self.order-1)
  end

end
