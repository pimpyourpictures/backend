class Customisation < ApplicationRecord
  has_one_attached :file
  belongs_to :project
  belongs_to :field
end
