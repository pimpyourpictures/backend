class FieldSetting < ApplicationRecord
  belongs_to :field_type_setting_type
  belongs_to :field
  has_one :template, :through => :field
end
