class Variant < ApplicationRecord
  belongs_to :template

  has_many :projects
  has_many :variant_specifics

  has_and_belongs_to_many :resolutions do
    def <<(new_item)
      super( Array(new_item) - proxy_association.owner.resolutions )
    end
  end

  def order_scope
    self.template.variants
  end
  include Rearrangeable

  def to_s
    name
  end
end
