class Project < ApplicationRecord
  after_initialize :init

  enum state: { waiting_for_assets: 0, submited: 1, finished: 2 }
  belongs_to :user
  belongs_to :variant

  has_many :customisations

  def init
    self.role ||= "waiting_for_assets"
  end

  def to_s
    name
  end
end
