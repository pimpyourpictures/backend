class Group < ApplicationRecord
  belongs_to :template

  has_many :fields
  has_many :variant_specifics, as: :variantable
  has_many :variants, through: :variant_specifics do
    def <<(new_item)
      super( Array(new_item) - proxy_association.owner.variants )
    end
  end

  def order_scope
    self.template.groups
  end
  include Rearrangeable

  def to_s
    name
  end
end
