class TemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :length, :actif
end
