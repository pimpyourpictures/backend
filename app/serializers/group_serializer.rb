class GroupSerializer < ActiveModel::Serializer
  attributes :id, :order, :name
  has_one :template
end
