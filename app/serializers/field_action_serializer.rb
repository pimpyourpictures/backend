class FieldActionSerializer < ActiveModel::Serializer
  attributes :id, :name, :hide_default, :hide_invert_if_empty
  has_one :field
end
