class CustomisationSerializer < ActiveModel::Serializer
  attributes :id, :index, :value
  has_one :project
  has_one :field
end
