class FieldTypeSettingTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_one :field_type
end
