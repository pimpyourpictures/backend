class VariantSerializer < ActiveModel::Serializer
  attributes :id, :filename, :name, :description, :actif
  has_one :template
end
