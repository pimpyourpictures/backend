class FieldTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
