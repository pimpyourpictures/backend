class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :state
  has_one :user
  has_one :variant
end
