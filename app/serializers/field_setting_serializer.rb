class FieldSettingSerializer < ActiveModel::Serializer
  attributes :id, :value
  has_one :field_type_setting_type
  has_one :field
end
