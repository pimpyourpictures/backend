class FieldSerializer < ActiveModel::Serializer
  attributes :id, :order, :name, :optional, :qtmin, :qtmax
  has_one :field_type
  has_one :group
  has_one :field
end
