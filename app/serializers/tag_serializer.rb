class TagSerializer < ActiveModel::Serializer
  attributes :id, :name, :highlighted
end
