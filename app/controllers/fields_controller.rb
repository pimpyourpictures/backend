class FieldsController < ApplicationController
  before_action :set_template
  before_action :set_group
  before_action :set_field, only: [:show, :update, :destroy]

  # POST /fields/rearrange
  # POST /fields/rearrange.json
  def rearrange
    @group.fields.find_by(order: params[:from]).rearrange(params[:to].to_i)
    @template
    render "templates/show.json"
  end

  # GET /fields
  # GET /fields.json
  def index
    @fields = @group.fields.all
  end

  # GET /fields/1
  # GET /fields/1.json
  def show
  end

  # POST /fields
  # POST /fields.json
  def create
    @field = @group.fields.new(field_params)

    if @field.save
      @template
      render "templates/show.json"
    else
      render json: @field.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /fields/1
  # PATCH/PUT /fields/1.json
  def update
    @field.update(field_params)

    variants = params[:variants].split(",")
    @field.variants.where.not(filename: variants).each do |variant|
      @field.variants.delete(variant)
    end
    variants.each do |variant_filename|
      variant = @template.variants.find_by(filename: variant_filename)
      if !variant.nil?
        @field.variants << variant
      end
    end

    @template
    render "templates/show.json"
  end

  # DELETE /fields/1
  # DELETE /fields/1.json
  def destroy
    @field.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:template_id])
    end

    def set_group
      @group = @template.groups.find(params[:group_id])
    end

    def set_field
      @field = @group.fields.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def field_params
      params.require(:field).permit(:order, :name, :field_type_id, :optional, :qtmin, :qtmax, :group_id, :field_id)
    end
end
