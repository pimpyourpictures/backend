class CustomisationsController < ApplicationController
  before_action :set_project
  before_action :set_customisation, only: [:show, :update, :destroy]

  # GET /customisations
  # GET /customisations.json
  def index
    @customisations = @project.customisations.all
  end

  # GET /customisations/1
  # GET /customisations/1.json
  def show
  end

  # POST /customisations
  # POST /customisations.json
  def create
    @customisation = @project.customisations.new(customisation_params)

    if @customisation.save
      render :show, status: :created, location: @customisation
    else
      render json: @customisation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /customisations/1
  # PATCH/PUT /customisations/1.json
  def update
    if @customisation.update(customisation_params)
      render :show, status: :ok, location: @customisation
    else
      render json: @customisation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /customisations/1
  # DELETE /customisations/1.json
  def destroy
    @customisation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:project_id])
    end

    def set_customisation
      @customisation = @project.customisations.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customisation_params
      params.require(:customisation).permit(:index, :value, :project_id, :field_id, :file)
    end
end
