class GroupsController < ApplicationController
  before_action :set_template
  before_action :set_group, only: [:show, :update, :destroy]

  # POST /groups/rearrange
  # POST /groups/rearrange.json
  def rearrange
    @template.groups.find_by(order: params[:from]).rearrange(params[:to].to_i)
    render "templates/show.json"
  end

  # GET /groups
  # GET /groups.json
  def index
    @groups = @template.groups.all
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = @template.groups.new(group_params)

    if @group.save
      @template
      render "templates/show.json"
    else
      render json: @group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    @group.update(group_params)
    variants = params[:variants].split(",")
    @group.variants.where.not(filename: variants).each do |variant|
      @group.variants.delete(variant)
    end
    variants.each do |variant_filename|
      variant = @template.variants.find_by(filename: variant_filename)
      if !variant.nil?
        @group.variants << variant
      end
    end

    @template
    render "templates/show.json"
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:template_id])
    end

    def set_group
      @group = @template.groups.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params
      params.require(:group).permit(:name, :variants)
    end
end
