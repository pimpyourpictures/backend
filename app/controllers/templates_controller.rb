class TemplatesController < ApplicationController
  require 'zip'
  before_action :set_template, only: [:show, :update, :destroy]

  # POST /groups/rearrange
  # POST /groups/rearrange.json
  def rearrange
    Template.find_by(order: params[:from]).rearrange(params[:to].to_i)
    @templates = Template.all
    render "templates/index.json"
  end

  # GET /templates
  # GET /templates.json
  def index
    @templates = Template.all
  end

  # GET /templates/1
  # GET /templates/1.json
  def show
  end

  # POST /templates
  # POST /templates.json
  def create
    @template = Template.new(template_params)

    if @template.save
      @templates = Template.all
      render "templates/index.json"
    else
      render json: @template.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /templates/1
  # PATCH/PUT /templates/1.json
  def update
    if @template.update(template_params)
      if template_params.key?(:projectfile)
        @template.variants.each do |variant|
          variant.resolutions.delete_all
        end
        filenames = []
        Zip::File.open(template_params[:projectfile].tempfile) do |zip_file|
          zip_file.each do |entry|
            splited_name = entry.name.split(".")
            if (splited_name.size==3 && splited_name.last=="blend")
              if res = Resolution.find_by(designator: splited_name[1])
                variant = @template.variants.find_by(filename: splited_name[0])
                if variant.nil?
                  variant = @template.variants.create(name: splited_name[0], filename: splited_name[0])
                end
                variant.resolutions << res
                filenames << splited_name[0]
              end
            end
          end
        end
        @template.variants.where.not(filename: filenames).update(filename: "", actif: false)
      end
      @template = Template.find(params[:id])
      render :show, status: :ok, location: @template
    else
      render json: @template.errors, status: :unprocessable_entity
    end
  end

  # DELETE /templates/1
  # DELETE /templates/1.json
  def destroy
    @template.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def template_params
      params.require(:template).permit(:name, :description, :length, :actif, :projectfile, :thumbnail, :preview)
    end
end
