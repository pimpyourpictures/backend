class FieldTypesController < ApplicationController
  before_action :set_field_type, only: [:show, :update, :destroy]

  # GET /field_types
  # GET /field_types.json
  def index
    @field_types = FieldType.all
  end

  # GET /field_types/1
  # GET /field_types/1.json
  def show
  end

  # POST /field_types
  # POST /field_types.json
  def create
    @field_type = FieldType.new(field_type_params)

    if @field_type.save
      render :show, status: :created, location: @field_type
    else
      render json: @field_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /field_types/1
  # PATCH/PUT /field_types/1.json
  def update
    if @field_type.update(field_type_params)
      render :show, status: :ok, location: @field_type
    else
      render json: @field_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /field_types/1
  # DELETE /field_types/1.json
  def destroy
    @field_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_field_type
      @field_type = FieldType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def field_type_params
      params.require(:field_type).permit(:name)
    end
end
