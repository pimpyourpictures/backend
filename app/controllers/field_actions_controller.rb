class FieldActionsController < ApplicationController
  before_action :set_template
  before_action :set_group
  before_action :set_field
  before_action :set_field_action, only: [:show, :update, :destroy]

  # GET /field_actions
  # GET /field_actions.json
  def index
    @field_actions = @field.field_actions.all
  end

  # GET /field_actions/1
  # GET /field_actions/1.json
  def show
  end

  # POST /field_actions
  # POST /field_actions.json
  def create
    @field_action = @field.field_actions.new(field_action_params)

    if @field_action.save
      render :show, status: :created, location: @field_action
    else
      render json: @field_action.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /field_actions/1
  # PATCH/PUT /field_actions/1.json
  def update
    if @field_action.update(field_action_params)
      render :show, status: :ok, location: @field_action
    else
      render json: @field_action.errors, status: :unprocessable_entity
    end
  end

  # DELETE /field_actions/1
  # DELETE /field_actions/1.json
  def destroy
    @field_action.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:template_id])
    end

    def set_group
      @group = @template.groups.find(params[:group_id])
    end

    def set_field
      @field = @group.fields.find(params[:field_id])
    end

    def set_field_action
      @field_action = @field.field_actions.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def field_action_params
      params.require(:field_action).permit(:name, :hide_default, :hide_invert_if_empty, :field_id)
    end
end
