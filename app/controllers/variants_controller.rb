class VariantsController < ApplicationController
  before_action :set_template
  before_action :set_variant, only: [:show, :update, :destroy, :merge]

  # POST /variants/merge
  # POST /variants/merge.json
  def merge
    @variant
    @mergewith = @template.variants.find(params[:merge_with])
    @variant.projects.update_all(variant_id: @mergewith.id)
    @variant.variant_specifics.update_all(variant_id: @mergewith.id)
    if params[:keep_description]
      @mergewith.update(description: @variant.description)
    end
    if params[:keep_name]
      @mergewith.update(name: @variant.name)
    end
    @variant.destroy
    @template
    render "templates/show.json"
  end

  # POST /variants/rearrange
  # POST /variants/rearrange.json
  def rearrange
    @template.variants.where.not(filename: "").find_by(order: params[:from]).rearrange(params[:to].to_i)
    render "templates/show.json"
  end

  # GET /variants
  # GET /variants.json
  def index
    @variants = @template.variants.all
  end

  # GET /variants/1
  # GET /variants/1.json
  def show
  end

  # POST /variants
  # POST /variants.json
  def create
    @variant = @template.variants.new(variant_params)

    if @variant.save
      render :show, status: :created, location: @variant
    else
      render json: @variant.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /variants/1
  # PATCH/PUT /variants/1.json
  def update
    if @variant.update(variant_params)
      @template
      render "templates/show.json"
    else
      render json: @variant.errors, status: :unprocessable_entity
    end
  end

  # DELETE /variants/1
  # DELETE /variants/1.json
  def destroy
    @variant.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:template_id])
    end

    def set_variant
      @variant = @template.variants.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def variant_params
      params.require(:variant).permit(:filename, :name, :description, :actif, :template_id)
    end
end
