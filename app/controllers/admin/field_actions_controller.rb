module Admin
  class FieldActionsController < ApplicationController
    before_action :set_field_action, only: [:update, :destroy]

    # POST /field_actions
    # POST /field_actions.json
    def create
      @field_action = FieldAction.new(field_action_params)
      if @field_action.save
        @template = @field_action.template
        render "admin/templates/show.json"
      else
        render json: @field_action.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /field_actions/1
    # PATCH/PUT /field_actions/1.json
    def update
      if @field_action.update(field_action_params)
        @template = @field_action.template
        render "admin/templates/show.json"
      else
        render json: @field_action.errors, status: :unprocessable_entity
      end
    end

    # DELETE /field_actions/1
    # DELETE /field_actions/1.json
    def destroy
      @field_action.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_field_action
        @field_action = FieldAction.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def field_action_params
        params.require(:field_action).permit(:name, :hide_default, :hide_invert_if_empty, :field_id)
      end
  end
end
