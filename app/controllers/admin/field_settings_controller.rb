module Admin
  class FieldSettingsController < ApplicationController
    before_action :set_field_setting, only: [:update, :destroy]

    # POST /field_settings
    # POST /field_settings.json
    def create
      @field_setting = FieldSetting.new(field_setting_params)
      if @field_setting.save
        @template = @field_setting.template
        render "admin/templates/show.json"
      else
        render json: @field_setting.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /field_settings/1
    # PATCH/PUT /field_settings/1.json
    def update
      if @field_setting.update(field_setting_params)
        @template = @field_setting.template
        render "admin/templates/show.json"
      else
        render json: @field_setting.errors, status: :unprocessable_entity
      end
    end

    # DELETE /field_settings/1
    # DELETE /field_settings/1.json
    def destroy
      @field_setting.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_field_setting
        @field_setting = FieldSetting.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def field_setting_params
        params.require(:field_setting).permit(:field_type_setting_type_id, :field_id, :value)
      end
  end
end
