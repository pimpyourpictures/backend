module Admin
  class GroupsController < ApplicationController
    before_action :set_group, only: [:update, :destroy, :rearrange]

    # POST /groups/rearrange
    # POST /groups/rearrange.json
    def rearrange
      @group.rearrange(params[:to].to_i)
      @template = @group.template
      render "admin/templates/show.json"
    end

    # POST /groups
    # POST /groups.json
    def create
      @group = Group.new(group_params)

      if @group.save
        @template = @group.template
        render "admin/templates/show.json"
      else
        render json: @group.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /groups/1
    # PATCH/PUT /groups/1.json
    def update
      @group.update(group_params)
      variants = params[:variants].split(",")
      @group.variants.where.not(id: variants).each do |variant|
        @group.variants.delete(variant)
      end
      @template = @group.template
      variants.each do |variant_id|
        variant = @template.variants.find(variant_id)
        if !variant.nil?
          @group.variants << variant
        end
      end

      @template
      render "admin/templates/show.json"
    end

    # DELETE /groups/1
    # DELETE /groups/1.json
    def destroy
      @group.destroy
    end

    private
      def set_group
        @group = Group.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def group_params
        params.require(:group).permit(:name, :variants, :template_id)
      end
  end
end
