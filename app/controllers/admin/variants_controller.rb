module Admin
  class VariantsController < ApplicationController
    before_action :set_variant, only: [:update, :destroy, :merge, :rearrange]

    # POST /variants/merge
    # POST /variants/merge.json
    def merge
      @mergewith = Variant.find(params[:merge_with])
      @variant.projects.update_all(variant_id: @mergewith.id)
      @variant.variant_specifics.update_all(variant_id: @mergewith.id)
      if params[:keep_description]
        @mergewith.update(description: @variant.description)
      end
      if params[:keep_name]
        @mergewith.update(name: @variant.name)
      end
      @variant.destroy
      @template = @mergewith.template
      render "admin/templates/show.json"
    end

    # POST /variants/rearrange
    # POST /variants/rearrange.json
    def rearrange
      @variant.rearrange(params[:to].to_i)
      @template = @variant.template
      render "admin/templates/show.json"
    end

    # POST /variants
    # POST /variants.json
    def create
      @variant = Variant.new(variant_params)

      if @variant.save
        @template = @variant.template
        render "admin/templates/show.json"
      else
        render json: @variant.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /variants/1
    # PATCH/PUT /variants/1.json
    def update
      if @variant.update(variant_params)
        @template = @variant.template
        render "admin/templates/show.json"
      else
        render json: @variant.errors, status: :unprocessable_entity
      end
    end

    # DELETE /variants/1
    # DELETE /variants/1.json
    def destroy
      @variant.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_variant
        @variant = Variant.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def variant_params
        params.require(:variant).permit(:filename, :name, :description, :actif, :template_id)
      end
  end
end
