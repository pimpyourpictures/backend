module Admin
  class FieldsController < ApplicationController
    before_action :set_field, only: [:update, :destroy, :rearrange]

    # POST /fields/rearrange
    # POST /fields/rearrange.json
    def rearrange
      @field.rearrange(params[:to].to_i)
      @template = @field.template
      render "admin/templates/show.json"
    end

    # POST /fields
    # POST /fields.json
    def create
      @field = Field.new(field_params)

      if @field.save
        @template = @field.template
        render "admin/templates/show.json"
      else
        render json: @field.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /fields/1
    # PATCH/PUT /fields/1.json
    def update
      @field.update(field_params)
      variants = params[:variants].split(",")
      @field.variants.where.not(id: variants).each do |variant|
        @field.variants.delete(variant)
      end
      @template = @field.template
      variants.each do |variant_id|
      variant = @template.variants.find(variant_id)
        if !variant.nil?
          @field.variants << variant
        end
      end

      @template
      render "admin/templates/show.json"
    end

    # DELETE /fields/1
    # DELETE /fields/1.json
    def destroy
      @field.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_field
        @field = Field.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def field_params
        params.require(:field).permit(:order, :name, :field_type_id, :optional, :qtmin, :qtmax, :group_id, :field_id)
      end
  end
end
