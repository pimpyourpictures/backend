class FieldSettingsController < ApplicationController
  before_action :set_template
  before_action :set_group
  before_action :set_field
  before_action :set_field_setting, only: [:show, :update, :destroy]

  # GET /field_settings
  # GET /field_settings.json
  def index
    @field_settings = @field.field_settings.all
  end

  # GET /field_settings/1
  # GET /field_settings/1.json
  def show
  end

  # POST /field_settings
  # POST /field_settings.json
  def create
    @field_setting = @field.field_settings.new(field_setting_params)

    if @field_setting.save
      render :show, status: :created, location: @field_setting
    else
      render json: @field_setting.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /field_settings/1
  # PATCH/PUT /field_settings/1.json
  def update
    if @field_setting.update(field_setting_params)
      render :show, status: :ok, location: @field_setting
    else
      render json: @field_setting.errors, status: :unprocessable_entity
    end
  end

  # DELETE /field_settings/1
  # DELETE /field_settings/1.json
  def destroy
    @field_setting.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:template_id])
    end

    def set_group
      @group = @template.groups.find(params[:group_id])
    end

    def set_field
      @field = @group.fields.find(params[:field_id])
    end

    def set_field_setting
      @field_setting = @field.field_settings.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def field_setting_params
      params.require(:field_setting).permit(:field_type_setting_type_id, :field_id, :value)
    end
end
