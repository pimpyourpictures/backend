class FieldTypeSettingTypesController < ApplicationController
  before_action :set_field_type
  before_action :set_field_type_setting_type, only: [:show, :update, :destroy]

  # GET /field_type_setting_types
  # GET /field_type_setting_types.json
  def index
    @field_type_setting_types = @field_type.field_type_setting_types.all
  end

  # GET /field_type_setting_types/1
  # GET /field_type_setting_types/1.json
  def show
  end

  # POST /field_type_setting_types
  # POST /field_type_setting_types.json
  def create
    @field_type_setting_type = @field_type.field_type_setting_types.new(field_type_setting_type_params)

    if @field_type_setting_type.save
      render :show, status: :created, location: @field_type_setting_type
    else
      render json: @field_type_setting_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /field_type_setting_types/1
  # PATCH/PUT /field_type_setting_types/1.json
  def update
    if @field_type_setting_type.update(field_type_setting_type_params)
      render :show, status: :ok, location: @field_type_setting_type
    else
      render json: @field_type_setting_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /field_type_setting_types/1
  # DELETE /field_type_setting_types/1.json
  def destroy
    @field_type_setting_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_field_type
      @field_type = FieldType.find(params[:field_type_id])
    end

    def set_field_type_setting_type
      @field_type_setting_type = @field_type.field_type_setting_types.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def field_type_setting_type_params
      params.require(:field_type_setting_type).permit(:name, :field_type_id)
    end
end
