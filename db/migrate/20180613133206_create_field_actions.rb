class CreateFieldActions < ActiveRecord::Migration[5.2]
  def change
    create_table :field_actions do |t|
      t.string :name
      t.boolean :hide_default, :default => false
      t.boolean :hide_invert_if_empty, :default => false
      t.references :field, foreign_key: true

      t.timestamps
    end
  end
end
