class CreateFields < ActiveRecord::Migration[5.2]
  def change
    create_table :fields do |t|
      t.integer :order
      t.string :name
      t.references :field_type, foreign_key: true
      t.boolean :optional, :default => false
      t.integer :qtmin
      t.integer :qtmax
      t.references :group, foreign_key: true
      t.references :field, foreign_key: true

      t.timestamps
    end
  end
end
