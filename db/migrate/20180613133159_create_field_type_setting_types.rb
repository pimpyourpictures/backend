class CreateFieldTypeSettingTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :field_type_setting_types do |t|
      t.string :name
      t.references :field_type, foreign_key: true

      t.timestamps
    end
  end
end
