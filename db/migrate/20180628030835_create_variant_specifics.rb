class CreateVariantSpecifics < ActiveRecord::Migration[5.2]
  def change
    create_table :variant_specifics do |t|
      t.references :variant, foreign_key: true
      t.references :variantable, polymorphic: true

      t.timestamps
    end
  end
end
