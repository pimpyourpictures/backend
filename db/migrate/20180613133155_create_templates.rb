class CreateTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :templates do |t|
      t.string :name, :default => "Dummy name"
      t.text :description
      t.integer :length, :default => 1
      t.boolean :actif, :default => false

      t.timestamps
    end
  end
end
