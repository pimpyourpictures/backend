class CreateResolutions < ActiveRecord::Migration[5.2]
  def change
    create_table :resolutions do |t|
      t.string :name
      t.string :designator

      t.timestamps
    end
  end
end
