class CreateFieldSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :field_settings do |t|
      t.references :field_type_setting_type, foreign_key: true
      t.references :field, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
