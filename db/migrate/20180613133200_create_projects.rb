class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.integer :state
      t.references :user, foreign_key: true
      t.references :variant, foreign_key: true

      t.timestamps
    end
  end
end
