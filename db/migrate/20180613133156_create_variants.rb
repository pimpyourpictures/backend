class CreateVariants < ActiveRecord::Migration[5.2]
  def change
    create_table :variants do |t|
      t.string :filename
      t.string :name, :default => "Dummy name"
      t.text :description
      t.boolean :actif, :default => false
      t.references :template, foreign_key: true

      t.timestamps
    end
  end
end
