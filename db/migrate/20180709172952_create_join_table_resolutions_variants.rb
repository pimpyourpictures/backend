class CreateJoinTableResolutionsVariants < ActiveRecord::Migration[5.2]
  def change
    create_join_table :resolutions, :variants do |t|
      # t.index [:resolution_id, :variant_id]
      # t.index [:variant_id, :resolution_id]
    end
  end
end
