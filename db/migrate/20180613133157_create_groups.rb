class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.integer :order
      t.string :name
      t.references :template, foreign_key: true

      t.timestamps
    end
  end
end
