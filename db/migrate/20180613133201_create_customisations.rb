class CreateCustomisations < ActiveRecord::Migration[5.2]
  def change
    create_table :customisations do |t|
      t.integer :index
      t.string :value
      t.references :project, foreign_key: true
      t.references :field, foreign_key: true

      t.timestamps
    end
  end
end
