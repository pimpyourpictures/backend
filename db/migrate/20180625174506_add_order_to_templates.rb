class AddOrderToTemplates < ActiveRecord::Migration[5.2]
  def change
    add_column :templates, :order, :integer
  end
end
