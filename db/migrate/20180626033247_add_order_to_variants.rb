class AddOrderToVariants < ActiveRecord::Migration[5.2]
  def change
    add_column :variants, :order, :integer
  end
end
