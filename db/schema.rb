# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_09_172952) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "customisations", force: :cascade do |t|
    t.integer "index"
    t.string "value"
    t.bigint "project_id"
    t.bigint "field_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_id"], name: "index_customisations_on_field_id"
    t.index ["project_id"], name: "index_customisations_on_project_id"
  end

  create_table "field_actions", force: :cascade do |t|
    t.string "name"
    t.boolean "hide_default", default: false
    t.boolean "hide_invert_if_empty", default: false
    t.bigint "field_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_id"], name: "index_field_actions_on_field_id"
  end

  create_table "field_settings", force: :cascade do |t|
    t.bigint "field_type_setting_type_id"
    t.bigint "field_id"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_id"], name: "index_field_settings_on_field_id"
    t.index ["field_type_setting_type_id"], name: "index_field_settings_on_field_type_setting_type_id"
  end

  create_table "field_type_setting_types", force: :cascade do |t|
    t.string "name"
    t.bigint "field_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_type_id"], name: "index_field_type_setting_types_on_field_type_id"
  end

  create_table "field_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fields", force: :cascade do |t|
    t.integer "order"
    t.string "name"
    t.bigint "field_type_id"
    t.boolean "optional", default: false
    t.integer "qtmin"
    t.integer "qtmax"
    t.bigint "group_id"
    t.bigint "field_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["field_id"], name: "index_fields_on_field_id"
    t.index ["field_type_id"], name: "index_fields_on_field_type_id"
    t.index ["group_id"], name: "index_fields_on_group_id"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "order"
    t.string "name"
    t.bigint "template_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["template_id"], name: "index_groups_on_template_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.integer "state"
    t.bigint "user_id"
    t.bigint "variant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_projects_on_user_id"
    t.index ["variant_id"], name: "index_projects_on_variant_id"
  end

  create_table "resolutions", force: :cascade do |t|
    t.string "name"
    t.string "designator"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "resolutions_variants", id: false, force: :cascade do |t|
    t.bigint "resolution_id", null: false
    t.bigint "variant_id", null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.boolean "highlighted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "templates", force: :cascade do |t|
    t.string "name", default: "Dummy name"
    t.text "description"
    t.integer "length", default: 1
    t.boolean "actif", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "access_token"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "variant_specifics", force: :cascade do |t|
    t.bigint "variant_id"
    t.string "variantable_type"
    t.bigint "variantable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["variant_id"], name: "index_variant_specifics_on_variant_id"
    t.index ["variantable_type", "variantable_id"], name: "index_variant_specifics_on_variantable_type_and_variantable_id"
  end

  create_table "variants", force: :cascade do |t|
    t.string "filename"
    t.string "name", default: "Dummy name"
    t.text "description"
    t.boolean "actif", default: false
    t.bigint "template_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order"
    t.index ["template_id"], name: "index_variants_on_template_id"
  end

  add_foreign_key "customisations", "fields"
  add_foreign_key "customisations", "projects"
  add_foreign_key "field_actions", "fields"
  add_foreign_key "field_settings", "field_type_setting_types"
  add_foreign_key "field_settings", "fields"
  add_foreign_key "field_type_setting_types", "field_types"
  add_foreign_key "fields", "field_types"
  add_foreign_key "fields", "fields"
  add_foreign_key "fields", "groups"
  add_foreign_key "groups", "templates"
  add_foreign_key "projects", "users"
  add_foreign_key "projects", "variants"
  add_foreign_key "variant_specifics", "variants"
  add_foreign_key "variants", "templates"
end
