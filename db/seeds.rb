# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: "nikolabjelo@gmail.com", password: "mnogosamlep")
User.create(email: "milanovic.ivan@gmail.com", password: "kuhclfccb")
User.create(email: "kejkzz@gmail.com", password: "pdppiulji")
User.create(email: "milan@gmail.com", password: "123456789")

FieldType.create(name: "set")
FieldType.create(name: "string",
  field_type_setting_types_attributes:[
    {name: "placeholder"},
    {name: "min_length"},
    {name: "max_length"}
  ])
FieldType.create(name: "integer",
  field_type_setting_types_attributes:[
    {name: "placeholder"},
    {name: "min"},
    {name: "max"}
  ])
FieldType.create(name: "picture",
  field_type_setting_types_attributes:[
    {name: "transparency"},
    {name: "min_ratio"},
    {name: "max_ratio"}
  ])
FieldType.create(name: "video",
  field_type_setting_types_attributes:[
    {name: "min_duration"},
    {name: "max_duration"},
    {name: "min_ratio"},
    {name: "max_ratio"}
  ])
FieldType.create(name: "sound",
  field_type_setting_types_attributes:[
    {name: "min_duration"},
    {name: "max_duration"}
  ])
FieldType.create(name: "color",
  field_type_setting_types_attributes:[
    {name: "placeholder"}
  ])

Resolution.create(name: "360p", designator: "360p")
Resolution.create(name: "720p", designator: "720p")
Resolution.create(name: "1080p", designator: "1080p")
