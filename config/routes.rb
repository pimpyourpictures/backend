Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/gestion', as: 'rails_admin'
  resources :projects do
    resources :customisations
  end

  resources :field_types do
    resources :field_type_setting_types
  end

  resources :tags
  resources :templates do
    post 'rearrange', on: :collection
    resources :variants do
      post 'rearrange', on: :collection
      post 'merge', on: :member
    end
    resources :groups do
      post 'rearrange', on: :collection
      resources :fields do
        post 'rearrange', on: :collection
        resources :field_actions
        resources :field_settings
      end
    end
  end

  namespace :admin do
    resources :templates do
      post 'rearrange', on: :member
    end
    resources :variants do
      post 'rearrange', on: :member
      post 'merge', on: :member
    end
    resources :groups do
      post 'rearrange', on: :member
    end
    resources :fields do
      post 'rearrange', on: :member
    end
    resources :field_actions
    resources :field_settings
  end

  devise_for :user, only: []

  post 'login', to: 'session#create'
end
